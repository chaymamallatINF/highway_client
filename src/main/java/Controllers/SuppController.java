/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import tn.esprit.pidev.service.PortfolioService;


/**
 * FXML Controller class
 *
 * @author LENOVO
 */
public class SuppController implements Initializable {

    @FXML
    private TextField idEntry;
    @FXML
    private Button deletTrade;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void supprimerTrade(ActionEvent event) throws NamingException {
    	String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
        Context context = new InitialContext();
   		
   		PortfolioService proxy=(PortfolioService) context.lookup(jndiName);
   	proxy.delete(Integer.valueOf(idEntry.getText()));	
    	
    }
    
}
