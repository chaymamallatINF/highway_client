/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.annotations.Parent;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.service.PortfolioInterface;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.*;
<<<<<<< HEAD

/**
 * FXML Controller class
 *
 * @author Win10-Space
 */
public class PortfolioController implements Initializable {

	@FXML
	private TextField strategytf;
	@FXML
	private TextField typetf;
	@FXML
	private TableView<Portfolio> tableviewrevenu = new TableView<Portfolio>();
	@FXML
	private TableColumn<?, ?> strategycol;
	@FXML
	private TableColumn<?, ?> typecol;
	ObservableList<Portfolio> data1 = FXCollections.observableArrayList();
	@FXML
	private Button addbtn;
	@FXML
	private Button modifbtn;
	@FXML
	private Button deletebtn;
	@FXML
	private TextField idportfoliotf;
	@FXML
	private Button rechercherbtn;
	@FXML
	private Button refreshbtn1;
	@FXML
	private Label somme;
	@FXML
	private Button stat;
	
	@FXML
	private AnchorPane pane;
	String idnew;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		try {
			afficherrevenu();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tableviewrevenu.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

			showPubDetails(newValue);

		});

	}

	private void afficherrevenu() throws NamingException {
		/// afficher /////////

		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx;
		try {
			ctx = new InitialContext();
			PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);
			typecol.setCellValueFactory(new PropertyValueFactory<>("type"));
			strategycol.setCellValueFactory(new PropertyValueFactory<>("category"));

			// System.out.println("bbbbbbbbb");
			Portfolio c = new Portfolio();
			List<Portfolio> o = proxy.getAllPortfolio();
			for (Portfolio e : o) {
				data1.add(e);
				// System.out.println("aaaaaaaa");
			}
			tableviewrevenu.setItems(data1);
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// **********************************************************************************
		FilteredList<Portfolio> filteredData = new FilteredList<>(data1, e -> true);
		idportfoliotf.setOnKeyReleased(e -> {
			idportfoliotf.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate((Predicate<? super Portfolio>) Portfolio -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();

					if (Portfolio.getType().toLowerCase().contains(CharSequence.class.cast(lowerCaseFilter))) {
						return true;
					}
					return false;

				});
			});
			SortedList<Portfolio> sortedData = new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(tableviewrevenu.comparatorProperty());
			tableviewrevenu.setItems(sortedData);

		});

	}

	@FXML
	private void add(ActionEvent event) throws NamingException {
		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context context = new InitialContext();

		PortfolioInterface proxy = (PortfolioInterface) context.lookup(jndiName);
		Portfolio t = new Portfolio();
		t.setType(String.valueOf(typetf.getText()));
		t.setCategory(String.valueOf(strategytf.getText()));
		proxy.addPortfolio(t);
		tableviewrevenu.getItems().clear();
		afficherrevenu();
		strategytf.setText("");
		typetf.setText("");

	}

	@FXML
	private void modifier(ActionEvent event) throws NamingException {

		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx = new InitialContext();
		PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);
		if (tableviewrevenu.getSelectionModel().getSelectedItem() != null) {
			Portfolio r = tableviewrevenu.getSelectionModel().getSelectedItem();
			r.setCategory((strategytf.getText()));
			r.setType((typetf.getText()));

			try {
				proxy.update(r);
				tableviewrevenu.getItems().clear();
				afficherrevenu();
				strategytf.setText("");
				typetf.setText("");
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	private void supprimer(ActionEvent event) throws NamingException {
		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx = new InitialContext();
		PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);

		proxy.delete(tableviewrevenu.getSelectionModel().getSelectedItem().getId());
		tableviewrevenu.getItems().clear();
		afficherrevenu();
		strategytf.setText("");
		typetf.setText("");

	}

	@FXML
	private void search(ActionEvent event) {
	}

	@FXML
	private void refreshrevenu(ActionEvent event) throws NamingException {

		afficherrevenu();
	}

	void showPubDetails(Portfolio e) {

		// System.out.println("id of event is " + e.getId_evenement());
		idnew = Integer.toString(e.getId());

		typetf.setText(e.getType());
		strategytf.setText(e.getCategory());

	}/*
		 * @FXML private void Update(ActionEvent event) throws NamingException {
		 * 
		 * String jndiName
		 * ="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CurrencyAccountRemote!tn.esprit.Finance_It_Team_server.services.ICurrencyAccount";
		 * Context ctx = new InitialContext(); ICurrencyAccount
		 * proxy=(ICurrencyAccount) ctx.lookup (jndiName); if
		 * (Tableview.getSelectionModel().getSelectedItem() != null) {
		 * CurrencyAccount r = Tableview.getSelectionModel().getSelectedItem();
		 * r.setAmount(Float.parseFloat(Amountentry.getText()));
		 * r.setTypecurrency(CurrencyTypeentry.getValue());
		 * 
		 * proxy.updateCurrecnyAccount(r); try { RemplirTable(); } catch
		 * (NamingException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * }
		 */

	@FXML
	private void passage(ActionEvent event) throws IOException {
		javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("../Viewers/Stat.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		Stage primaryStage = (Stage) pane.getScene().getWindow();
		primaryStage.close();
	}
=======
import FXMLControllers.LoginController;
/**
 * FXML Controller class
 *
 * @author Win10-Space
 */
public class PortfolioController implements Initializable {

	@FXML
	private TextField strategytf;
	@FXML
	private TextField typetf;
	@FXML 
	TableView<Portfolio> tableviewrevenu = new TableView<Portfolio>();
	@FXML
	private TableColumn<?, ?> strategycol;
	@FXML
	private TableColumn<?, ?> typecol;
	ObservableList<Portfolio> data1 = FXCollections.observableArrayList();
	@FXML
	private Button addbtn;
	@FXML
	private Button modifbtn;
	@FXML
	private Button deletebtn;
	@FXML
	private TextField idportfoliotf;
	@FXML
	private Button rechercherbtn;
	@FXML
	private Button refreshbtn1;
	@FXML
	private Label somme;
	@FXML
	private Button stat;
	
	@FXML
	private AnchorPane pane;
	@FXML
	private AnchorPane pane1;
	String idnew;
	static  Portfolio x;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		try {
			afficherrevenu();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tableviewrevenu.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

			showPubDetails(newValue);

		});
		

		
	}

	private void afficherrevenu() throws NamingException {
		/// afficher /////////

		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx;
		try {
			ctx = new InitialContext();
			PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);
			typecol.setCellValueFactory(new PropertyValueFactory<>("type"));
			strategycol.setCellValueFactory(new PropertyValueFactory<>("category"));

			// System.out.println("bbbbbbbbb");
			Portfolio c = new Portfolio();
			
			List<Portfolio> o = proxy.getAllPortfolio(LoginController.userConnected.getId());
			
			
				for (Portfolio e : o)
			{//System.out.println(e.getUser());
			//System.out.println(LoginController.userConnected.getId());
					//if(e.getUser().getId()==LoginController.userConnected.getId()){
				
				data1.add(e);
				// System.out.println("aaaaaaaa");
			}
			tableviewrevenu.setItems(data1);//}
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// **********************************************************************************
		FilteredList<Portfolio> filteredData = new FilteredList<>(data1, e -> true);
		idportfoliotf.setOnKeyReleased(e -> {
			idportfoliotf.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate((Predicate<? super Portfolio>) Portfolio -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();

					if (Portfolio.getType().toLowerCase().contains(CharSequence.class.cast(lowerCaseFilter))) {
						return true;
					}
					return false;

				});
			});
			SortedList<Portfolio> sortedData = new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(tableviewrevenu.comparatorProperty());
			tableviewrevenu.setItems(sortedData);

		});

	}

	@FXML
	private void add(ActionEvent event) throws NamingException {
		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context context = new InitialContext();

		PortfolioInterface proxy = (PortfolioInterface) context.lookup(jndiName);
		Portfolio t = new Portfolio();
		t.setType(String.valueOf(typetf.getText()));
		t.setCategory(String.valueOf(strategytf.getText()));
		t.setUser(LoginController.userConnected);
		proxy.addPortfolio(t);
		tableviewrevenu.getItems().clear();
		afficherrevenu();
		strategytf.setText("");
		typetf.setText("");

	}

	@FXML
	private void modifier(ActionEvent event) throws NamingException {

		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx = new InitialContext();
		PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);
		if (tableviewrevenu.getSelectionModel().getSelectedItem() != null) {
			Portfolio r = tableviewrevenu.getSelectionModel().getSelectedItem();
			r.setCategory((strategytf.getText()));
			r.setType((typetf.getText()));

			try {
				proxy.update(r);
				tableviewrevenu.getItems().clear();
				afficherrevenu();
				strategytf.setText("");
				typetf.setText("");
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	private void supprimer(ActionEvent event) throws NamingException {
		String jndiName = "pidev-ear/pidev-ejb/PortfolioService!tn.esprit.pidev.service.PortfolioInterface";
		Context ctx = new InitialContext();
		PortfolioInterface proxy = (PortfolioInterface) ctx.lookup(jndiName);

		 Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are You Sure Delete Data ?", ButtonType.YES, ButtonType.NO);
	        alert.showAndWait();
	        if (alert.getResult() == ButtonType.YES){
		//Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are You Sure Delete Data ?");
		proxy.delete(tableviewrevenu.getSelectionModel().getSelectedItem().getId());
		tableviewrevenu.getItems().clear();
		afficherrevenu();}
	        else{
	        	
	        	
	        }
		strategytf.setText("");
		typetf.setText("");
	        
	}

	@FXML
	private void search(ActionEvent event) throws IOException{
		Portfolio p = new Portfolio();
		Portfolio c = tableviewrevenu.getItems().get(tableviewrevenu.getSelectionModel().getSelectedIndex());
		x=c;
		if((x.getType().equals("defensive"))||(x.getType().equals("aggressive"))){
			javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("../Viewers/Option.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			Stage primaryStage = (Stage) pane1.getScene().getWindow();
			primaryStage.close();}
		if((x.getType().equals("income"))||(x.getType().equals("speculative"))){
			javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("../Viewers/Currency.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			Stage primaryStage = (Stage) pane1.getScene().getWindow();
			primaryStage.close();}
		
	
	}

	@FXML
	private void refreshrevenu(ActionEvent event) throws NamingException {

		afficherrevenu();
	}

	void showPubDetails(Portfolio e) {

		// System.out.println("id of event is " + e.getId_evenement());
		idnew = Integer.toString(e.getId());

		typetf.setText(e.getType());
		strategytf.setText(e.getCategory());

	}

	@FXML
	private void passage(ActionEvent event) throws IOException {
		javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("../Viewers/Stat.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		Stage primaryStage = (Stage) pane.getScene().getWindow();
		
	}
	
	
>>>>>>> branch 'master' of https://gitlab.com/chaymamallatINF/highway_client.git

}
//}