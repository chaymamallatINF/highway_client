package FXMLControllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;	
import javafx.scene.input.InputEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserServiceRemote;
import javafx.collections.FXCollections;
import javafx.beans.property.SimpleStringProperty;

public class FindUsersController implements Initializable {
   
	   @FXML
	    private TableColumn<User, String> colmail;

	    @FXML
	    private TableView<User> tableUsres;

	    @FXML
	    private Button btnLogout;

	    @FXML
	    private TableColumn<User, String> coloptions;

	    @FXML
	    private TableColumn<User, String> collastname;



	    @FXML
	    private TableColumn<User, String> colfirstname;

	    @FXML
	    private TableColumn<User, String>colnumber;

	    @FXML
	    private TableColumn<User, String>coladdress;

	    @FXML
	    private TextField FindField;

	    @FXML
	    private Button btnBlock;
UserServiceRemote us ;
private ObservableList<User> userList = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */


    public void initialize(URL url, ResourceBundle rb) {
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		UserServiceRemote us=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
			us = (UserServiceRemote) context.lookup(jndiName);
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
 userList.addAll(us.getTrader()) ;
 coladdress.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getAddress().toString()));
 colfirstname.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getFirst_name().toString()));
 collastname.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getLast_name().toString()));
colmail.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getMail().toString()));
colnumber.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getTel().toString()));
tableUsres.setItems(userList);
    
    }    

    @FXML
    void Blcokbtn(ActionEvent event) {
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		UserServiceRemote us=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
			us = (UserServiceRemote) context.lookup(jndiName);
		} catch
	(NamingException e1) {
			e1.printStackTrace();
		}
    	us.blockUser(tableUsres.getSelectionModel().getSelectedItem().getId());
    	
    	userList.clear();
    	
    	userList.addAll(us.rech(FindField.getText()));
    	tableUsres.setItems(userList);
    	

    }
    

    @FXML
    void Logoutbtn(ActionEvent event) throws IOException {
    	Stage stage = (Stage) btnBlock.getScene().getWindow();
      	Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();


    }

    @FXML
    void rech(InputEvent event) {
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		UserServiceRemote us=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
			us = (UserServiceRemote) context.lookup(jndiName);
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
    	userList.clear();
    	
userList.addAll(us.rech(FindField.getText()));
 tableUsres.setItems(userList);
    }
  

}
