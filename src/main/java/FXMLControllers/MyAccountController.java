package FXMLControllers;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.time.*;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tn.esprit.pidev.services.UserServiceRemote;
import javafx.fxml.FXMLLoader;

public class MyAccountController implements Initializable {
    @FXML
    private Button btnLogout;
    @FXML
    private Label adress_label;

    @FXML
    private Button btnDelete;

    @FXML
    private Label number_label;

    @FXML
    private Label mail_label;

    @FXML
    private Button btnUpdate;

    @FXML
    private Label first_name_label;

    @FXML
    private Label Date_of_birth_label;

    @FXML
    private Label Last_name_label;
    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
    	
    	first_name_label.setText(LoginController.userConnected.getFirst_name());
    	Last_name_label.setText(LoginController.userConnected.getLast_name());
     Date_of_birth_label.setText(LocalDate.of(LoginController.userConnected.getDate_of_birth().getYear()+1900, LoginController.userConnected.getDate_of_birth().getMonth()+1, LoginController.userConnected.getDate_of_birth().getDate()).toString());
    	adress_label.setText(LoginController.userConnected.getAddress());
      mail_label.setText(LoginController.userConnected.getMail());
      number_label.setText(LoginController.userConnected.getTel());
    }    
    

    @FXML
    void Updatebtn(ActionEvent event) throws IOException {
    	  Stage stage = (Stage) btnUpdate.getScene().getWindow();
          Parent root = FXMLLoader.load(getClass().getResource("/views/ModifCompte.fxml"));

           Scene scene = new Scene(root);

           stage.setScene(scene);

           stage.show();
    }
    @FXML
    void Deletebtn(ActionEvent event) throws IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation ??");
    	alert.setHeaderText("You are deleting your account");
    	alert.setContentText("Are yous sure ?");
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK){
    		Context context=null;
    		try {
    			context = new InitialContext();
    		} catch (NamingException e1) {
    			e1.printStackTrace();
    		}
    		UserServiceRemote us=null;
    		try {
    			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
    			us = (UserServiceRemote) context.lookup(jndiName);
    		} catch (NamingException e1) {
    			e1.printStackTrace();
    		}
    		us.deleteUserByid(LoginController.userConnected.getId());
    		LoginController.userConnected=null;
    		Stage stage = (Stage) btnLogout.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

             Scene scene = new Scene(root);

             stage.setScene(scene);

             stage.show();
    		
    	} 
    }

    @FXML
    void Logoubtn(ActionEvent event) throws IOException {
    	LoginController.userConnected=null;
    	 Stage stage = (Stage) btnLogout.getScene().getWindow();
         Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

          Scene scene = new Scene(root);

          stage.setScene(scene);

          stage.show();

    }


}
