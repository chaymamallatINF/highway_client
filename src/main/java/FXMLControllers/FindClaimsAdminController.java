package FXMLControllers;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;	
import javafx.scene.input.InputEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import tn.esprit.pidev.entity.Reclamation;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.ReclamationServiceRemote;
import tn.esprit.pidev.services.UserServiceRemote;
import javafx.collections.FXCollections;
import javafx.stage.Modality;
import javafx.scene.control.Alert;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TextArea;

public class FindClaimsAdminController implements Initializable {
	@FXML
    private TableColumn<Reclamation, String> colcontent;

    @FXML
    private TableColumn<Reclamation, String> coloption;

    @FXML
    private TextArea response_field;

    @FXML
    private Button btnDelete;

    @FXML
    private Button sendbtn;

    @FXML
    private TableColumn<Reclamation,String> colstatus;

    @FXML
    private TableColumn<Reclamation, String> coldate;

    @FXML
    private TextField FindField;

    @FXML
    private TableView<Reclamation> tableclaims;

    @FXML
    private TableColumn<Reclamation, String> colresponse;

    @FXML
    private TextArea content_filed;
    @FXML
    private Button btnActualize;
    @FXML
    private Button logoutbtn;


    private ObservableList<Reclamation> ClaimsList = FXCollections.observableArrayList();
	   
    ReclamationServiceRemote CS;
    public void initialize(URL url, ResourceBundle rb) {
		String jndiName ="pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
    	Context context = null;
    	try {
    		context = new InitialContext();
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	try {
    		 CS = (ReclamationServiceRemote)context.lookup(jndiName);
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		}
    	
    	 ClaimsList.addAll(CS.getReclamation());
    	 colcontent.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getContent().toString()));
    	 coldate.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getClaimDate().toString()));
    	 coloption.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getOption().toString()));
    	 
    	 colresponse.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getResponse().toString()));
    	 
    	 colstatus.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getStatus().toString()));

         tableclaims.setItems(ClaimsList);
    	 
    }
    
    @FXML
    void rech(InputEvent event) {
    	String jndiName ="pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
    	Context context = null;
    	try {
    		context = new InitialContext();
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	try {
    		 CS = (ReclamationServiceRemote)context.lookup(jndiName);
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		}
    ClaimsList.clear();
    	
ClaimsList.addAll(CS.rech(FindField.getText()));
 tableclaims.setItems(ClaimsList);

    }

    @FXML
    void Actualizebtn(ActionEvent event) {
    	String jndiName ="pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
    	Context context = null;
    	try {
    		context = new InitialContext();
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	try {
    		 CS = (ReclamationServiceRemote)context.lookup(jndiName);
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		}
    	content_filed.setText(tableclaims.getSelectionModel().getSelectedItem().getContent());	
    	response_field.setText(tableclaims.getSelectionModel().getSelectedItem().getResponse());
    	
    	int i=tableclaims.getSelectionModel().getSelectedItem().getId();

    }
   

    @FXML
    void btnsend(ActionEvent event) {
    	String jndiName ="pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
    	Context context = null;
    	try {
    		context = new InitialContext();
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	try {
    		 CS = (ReclamationServiceRemote)context.lookup(jndiName);
    	} catch (NamingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		}
    	
    	Reclamation c=tableclaims.getSelectionModel().getSelectedItem();
    	int Claim_id=tableclaims.getSelectionModel().getSelectedItem().getId();
    	if(response_field.getText().isEmpty()){
    	    Alert missingFields = new Alert(Alert.AlertType.INFORMATION);
            missingFields.setHeaderText(null);
            missingFields.setContentText("veuillez saisir une reponse");
            missingFields.initModality(Modality.APPLICATION_MODAL);
            missingFields.showAndWait();
    		
    	}else{
    		CS.responseReclamation(Claim_id, response_field.getText());
       	 colresponse.setCellValueFactory( cellData-> new SimpleStringProperty(cellData.getValue().getResponse().toString()));

    		
    	     Alert missingFields = new Alert(Alert.AlertType.INFORMATION);
             missingFields.setHeaderText(null);
             missingFields.setContentText("traite avec succes");
             missingFields.initModality(Modality.APPLICATION_MODAL);
             missingFields.showAndWait();
             ClaimsList.clear();
         	
             ClaimsList.addAll(CS.getReclamation());
              tableclaims.setItems(ClaimsList);   
    	}
    	

    }

    @FXML
    void Deletebtn(ActionEvent event) {
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		ReclamationServiceRemote CS=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
			CS = (ReclamationServiceRemote) context.lookup(jndiName);
		} catch
	(NamingException e1) {
			e1.printStackTrace();
		}
    	CS.deleteReclamationById(tableclaims.getSelectionModel().getSelectedItem().getId());
    	
    	ClaimsList.clear();
    	
    	ClaimsList.addAll(CS.rech(FindField.getText()));
    	tableclaims.setItems(ClaimsList);


    }

    @FXML
    void btnlogout(ActionEvent event) throws IOException {
    	Stage stage = (Stage) logoutbtn.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

         Scene scene = new Scene(root);

         stage.setScene(scene);

         stage.show();


    }

}



