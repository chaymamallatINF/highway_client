package FXMLControllers;

import javafx.event.ActionEvent;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.xml.soap.Text;

import org.controlsfx.control.Notifications;

import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.image.ImageView;
import tn.esprit.pidev.entity.Type;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserService;
import tn.esprit.pidev.services.UserServiceRemote;



public class AcceuilController implements Initializable {
	public static User userConnected; 
	 @FXML
	    private Button Claimbtn;

	    @FXML
	    private Button accountbtn;

	
@Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }   


@FXML
void btnAccount(ActionEvent event) throws IOException {
	Stage stage = (Stage) accountbtn.getScene().getWindow();
    Parent root = FXMLLoader.load(getClass().getResource("/views/Myaccount.fxml"));

     Scene scene = new Scene(root);

     stage.setScene(scene);

     stage.show();


}

@FXML
void btnClaim(ActionEvent event) throws IOException {


	User u = new User();
	
		
		if(u != null)
		{
			
			userConnected=u;
			
			if(u.getType()==Type.Trader)
			{
				if(u.isActive())
				{
			
	

	      Stage stage = (Stage) Claimbtn.getScene().getWindow();
	       
	      Parent root = FXMLLoader.load(getClass().getResource("/views/ClaimsTrader.fxml"));
           Scene scene = new Scene((Parent) root);
           stage.setScene(scene);
           stage.centerOnScreen();
           stage.setResizable(true);
           stage.show();
				}
				else {
					Notifications notificationBuilder = Notifications.create().title("")
							.text("account banned ").darkStyle().graphic(null).hideAfter(Duration.seconds(5))
							.position(Pos.BOTTOM_RIGHT);
					notificationBuilder.showError();
					
				}
		    	
			
			}
		
			else if (u.getType()==Type.Admin) {
				
			}
			{
				Stage stage = (Stage) Claimbtn.getScene().getWindow();
			       
			      Parent root = FXMLLoader.load(getClass().getResource("/views/ClaimsAdmin.fxml"));
	               Scene scene = new Scene((Parent) root);
	              // stage.setScene(scene);
	              // stage.centerOnScreen();
	            //   stage.setResizable(true);
	               stage.show();
			}
		}
		
		
	}



}
