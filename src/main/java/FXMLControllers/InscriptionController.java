package FXMLControllers;


import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;

import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;

import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javafx.geometry.Pos;
import javafx.util.Duration;

import org.controlsfx.control.Notifications;


import javafx.fxml.Initializable;

import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserServiceRemote;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.PasswordField;
import javafx.scene.control.DatePicker;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.InputEvent;
import java.util.Date;
import java.util.Properties;
 public class InscriptionController implements Initializable {
	 
	 @FXML
	    private TextField txtnumber;
	    @FXML
	    private TextField txtFirst_name;

	    @FXML
	    private TextField txtemail;

	    @FXML
	    private TextField txtLast_name;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TextField txtaddress;
	    
	    @FXML
	    private PasswordField txtConfirmationPwd;
	    @FXML
	    private DatePicker Date_of_birth;

	    @FXML
	    private Text Date_of_birthAlert;

	    @FXML
	    private Text Last_nameAlert;
	    @FXML
	    private Text pwdAlert;
	    @FXML
	    private Text emailAlert;

	    @FXML
	    private Text first_nameAlert;

	    @FXML
	    private Text ConfirmationAlert;
	    @FXML
	    private ProgressBar pwdprog;
	    @FXML
	    private Label mail_label;

	    @FXML
	    private Label Date_of_birth_label;
	    @FXML
	    private Label addressLabel;
	    @FXML
	    private Label LastNameLabel;

	    @FXML
	    private Label FirstNameLabel;

	    @FXML
	    private Label NumberLabel;
	    UserServiceRemote us;
<<<<<<< HEAD
	    
=======
>>>>>>> branch 'master' of https://gitlab.com/chaymamallatINF/highway_client.git

	    @FXML
	    private PasswordField txtpwd;
	    boolean inscrire = false;
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		us=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
			us = (UserServiceRemote) context.lookup(jndiName);
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
    } 
	User user=new User();


	
    
    @FXML
    private void passwordVerifAction(InputEvent event) 
    {
        ConfirmationAlert.setText("");
      int maj=0, min=0,number=0,caracSp=0,letter=0;
          int securityCount=0;
        securityCount=txtpwd.getText().length();
        if(securityCount>7)
            securityCount=7;
        for(int i = 0; i<txtpwd.getText().length();i++)
        {
        if(Character.isUpperCase(txtpwd.getText().charAt(i)))
            maj++;
        else if (Character.isLowerCase(txtpwd.getText().charAt(i)))
            min++;
        else if(Character.isDigit(txtpwd.getText().charAt(i)))
            number++;
        else if(Character.isWhitespace(txtpwd.getText().charAt(i)))
            caracSp++;
            else
            caracSp++;
        
        }
        letter=maj+min;
        if(maj<min)
            min=maj;
        if(min>5)
            securityCount+=5;
        else
            securityCount+=min;
        if(letter<number)
            number=letter;
        if(number>5)
            securityCount+=5;
        else
            securityCount+=number;
        if(caracSp>4)
            securityCount+=4;
        else
            securityCount+=number;
        pwdprog.setVisible(true);
                if(securityCount!=0){
                if(securityCount<8)
                {
                	pwdAlert.setText("Faible");
                   
                }
                else if (securityCount<15)
                {
                	pwdAlert.setText("Moyen");
                }
                else 
                	pwdAlert.setText("Fort");
                }
                else
                	pwdAlert.setText("");
                pwdprog.setProgress((double) securityCount/21);
     
    }
  
    @FXML
    void registerbtn(ActionEvent event) throws IOException {
  
    	inscrire = true;
    	if (!txtpwd.getText().equals(txtConfirmationPwd.getText()))
    	{
    		inscrire = false;
    		ConfirmationAlert.setText("The two password are not the same !!");
    	}
    	if(us.isExist(txtemail.getText()))
    	{
    		inscrire = false;
    		
    	}
    	if(new Date().compareTo(new java.util.Date(Date_of_birth.getValue().getYear()-1900, Date_of_birth.getValue().getMonthValue()-1, Date_of_birth.getValue().getDayOfMonth()))<=0)
		{
	Notifications notificationBuilder = Notifications.create().title("")
			.text("welcome back from the future ").darkStyle().graphic(null).hideAfter(Duration.seconds(5))
			.position(Pos.BOTTOM_RIGHT);
	notificationBuilder.showWarning();
		inscrire = false;
		}
    	 	
		if(inscrire){
    	user.setFirst_name(txtFirst_name.getText());
    	user.setLast_name(txtLast_name.getText());
    	user.setDate_of_birth(new java.util.Date(Date_of_birth.getValue().getYear()-1900, Date_of_birth.getValue().getMonthValue()-1, Date_of_birth.getValue().getDayOfMonth()));
 	user.setAddress(txtaddress.getText());
    	user.setMail(txtemail.getText());
    	user.setTel(txtnumber.getText());
    	user.setPwd(txtpwd.getText());

    	us.ajouterUser(user);
    	Notifications notificationBuilder = Notifications.create().title("")
				.text("welcome in our applicationj").darkStyle().graphic(null).hideAfter(Duration.seconds(5))
				.position(Pos.BOTTOM_RIGHT);
		notificationBuilder.showConfirm();
    			
		}

        final String username = "chayma.mallat@esprit.tn";
final String password = "yosr12800063";

Properties props = new Properties();
props.put("mail.smtp.auth", "true");
props.put("mail.smtp.starttls.enable", "true");
props.put("mail.smtp.host", "smtp.gmail.com");
props.put("mail.smtp.port", "587");

Session session = Session.getInstance(props,
  new javax.mail.Authenticator() {
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
  });

try {

	Message message = new MimeMessage(session);
	message.setFrom(new InternetAddress("chayma.mallat@esprit.tn"));
	message.setRecipients(Message.RecipientType.TO,
		InternetAddress.parse(txtemail.getText()));
	message.setSubject("Code de validation");
	message.setText("Salut Monsieur "+txtFirst_name.getText()+" "+txtLast_name.getText()+".\n\n Voici votre code de validation :123 ");

	Transport.send(message);

	

} catch (MessagingException e) {
	throw new RuntimeException(e);
}        
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

         Scene scene = new Scene(root);

         stage.setScene(scene);

         stage.show();

    

    }
    
    @FXML
    void firstK(InputEvent event) {
    	FirstNameLabel.setText(txtFirst_name.getText());
    	
    }

    @FXML
    void firstL(InputEvent event) {
    	LastNameLabel.setText(txtLast_name.getText());
    } 

    @FXML
    void firstA(InputEvent event) {
    	addressLabel.setText(txtaddress.getText());
    }

    @FXML
    void firstE(InputEvent event) {
    	mail_label.setText(txtemail.getText());
    	if(!us.isExist(txtemail.getText()))
    	{
    		emailAlert.setText("This mail adress is available");
    		
    	}
    		
    	else  {
    		emailAlert.setText("This mail adress is used by another user, please change !");
    		
    	}
    }

    @FXML
    void firstN(InputEvent event) {

if(!Character.isDigit(txtnumber.getText().charAt(txtnumber.getLength()-1)))
		{
	txtnumber.deletePreviousChar();
		}
NumberLabel.setText(txtnumber.getText());
    
   
   
    
    } 
    
@FXML
void verifDate(ActionEvent event)
{
	if(new Date().compareTo(new java.util.Date(Date_of_birth.getValue().getYear()-1900, Date_of_birth.getValue().getMonthValue()-1, Date_of_birth.getValue().getDayOfMonth()))<=0)
			{
    	Notifications notificationBuilder = Notifications.create().title("")
				.text("welcome back from the future ").darkStyle().graphic(null).hideAfter(Duration.seconds(5))
				.position(Pos.BOTTOM_RIGHT);
		notificationBuilder.showWarning();
			}
	
	
	

}

}
