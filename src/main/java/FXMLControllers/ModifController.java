package FXMLControllers;


import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;

import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.controlsfx.control.Notifications;

import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.fxml.FXMLLoader;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserServiceRemote;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.DatePicker;
import javafx.scene.text.Text;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.InputEvent;
import java.util.Date;
 public class ModifController implements Initializable {
	 @FXML
	    private TextField txtnumber;

	    @FXML
	    private Text ConfirmationAlert;

	    @FXML
	    private TextField txtFirst_name;

	    @FXML
	    private TextField txtemail;

	    @FXML
	    private DatePicker Date_of_birth;

	    @FXML
	    private ProgressBar pwdprog;

	    @FXML
	    private TextField txtLast_name;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private PasswordField confpwd;

	    @FXML
	    private PasswordField newpxdfield;
	    @FXML
	    private PasswordField oldpwd;
	    @FXML
	    private Text pwdalert;



	    @FXML
	    private TextField txtaddress;
	    @FXML
	    private Text msg;

	    @FXML
	    private Button cancelbtn;
	    
	    UserServiceRemote us;
    public void initialize(URL url, ResourceBundle rb) {
       txtFirst_name.setText(LoginController.userConnected.getFirst_name());
       txtLast_name.setText(LoginController.userConnected.getLast_name());
       Date_of_birth.setValue(LocalDate.of(LoginController.userConnected.getDate_of_birth().getYear()+1900, LoginController.userConnected.getDate_of_birth().getMonth()+1, LoginController.userConnected.getDate_of_birth().getDate()));
       txtaddress.setText(LoginController.userConnected.getAddress());
       txtemail.setText(LoginController.userConnected.getMail());
       txtnumber.setText(LoginController.userConnected.getTel());
       
    } 
	
    @FXML
    void modifbtn(ActionEvent event) throws IOException {
    	
    	boolean modif=true;
    	if(!oldpwd.getText().isEmpty()){
    		if(!newpxdfield.getText().isEmpty()){
    			
    			
    			if(!newpxdfield.getText().equals(confpwd.getText())){
    				msg.setText("Passwords do not match");
    				modif=false;
    				
    			}else{
    				
    				if(!oldpwd.getText().equals(LoginController.userConnected.getPwd())){
    					
    					modif=false;
    					msg.setText("the old password is false");
    					
    				}
    				if(new Date().compareTo(new java.util.Date(Date_of_birth.getValue().getYear()-1900, Date_of_birth.getValue().getMonthValue()-1, Date_of_birth.getValue().getDayOfMonth()))<=0)
    				{
    			Notifications notificationBuilder = Notifications.create().title("")
    					.text("welcome back from the future ").darkStyle().graphic(null).hideAfter(Duration.seconds(5))
    					.position(Pos.BOTTOM_RIGHT);
    			notificationBuilder.showWarning();
    				modif = false;
    				}
<<<<<<< HEAD
    				
=======
    				//ki jit nenzel update
>>>>>>> branch 'master' of https://gitlab.com/chaymamallatINF/highway_client.git
    			}
    		}
    	}
    	if(modif){
    		LoginController.userConnected.setFirst_name(txtFirst_name.getText());
    		LoginController.userConnected.setLast_name(txtLast_name.getText());
    		LoginController.userConnected.setDate_of_birth(new java.util.Date(Date_of_birth.getValue().getYear()-1900, Date_of_birth.getValue().getMonthValue()-1, Date_of_birth.getValue().getDayOfMonth()));
    		LoginController.userConnected.setAddress(txtaddress.getText());
    		LoginController.userConnected.setMail(txtemail.getText());
    		LoginController.userConnected.setPwd(newpxdfield.getText());
    	LoginController.userConnected.setTel(txtnumber.getText());
    	Context context=null;
		try {
			context = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		UserServiceRemote us=null;
		try {
			String jndiName="pidev-ear/pidev-ejb/UserService!tn.esprit.pidev.services.UserServiceRemote";
			us = (UserServiceRemote) context.lookup(jndiName);
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
	
	
		us.UpdateUser(LoginController.userConnected.getAddress(), LoginController.userConnected.getPwd(), LoginController.userConnected.getLast_name(),LoginController.userConnected.getTel(), LoginController.userConnected.getFirst_name(), LoginController.userConnected.getDate_of_birth(), LoginController.userConnected.getMail(), LoginController.userConnected.getId());
    	Stage stage = (Stage) cancelbtn.getScene().getWindow();
      	Parent root = FXMLLoader.load(getClass().getResource("/views/MyAccount.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();	
       
    	}
    	
    	 
      

    }
    @FXML
    void Deletebtn(ActionEvent event) throws IOException {
    	
    	Stage stage = (Stage) cancelbtn.getScene().getWindow();
      	Parent root = FXMLLoader.load(getClass().getResource("/views/MyAccount.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();

    }




    @FXML
    private void passwordVerification(InputEvent event) 
    {
        
      int maj=0, min=0,number=0,caracSp=0,letter=0;
          int securityCount=0;
        securityCount=newpxdfield.getText().length();
        if(securityCount>7)
            securityCount=7;
        for(int i = 0; i<newpxdfield.getText().length();i++)
        {
        if(Character.isUpperCase(newpxdfield.getText().charAt(i)))
            maj++;
        else if (Character.isLowerCase(newpxdfield.getText().charAt(i)))
            min++;
        else if(Character.isDigit(newpxdfield.getText().charAt(i)))
            number++;
        else if(Character.isWhitespace(newpxdfield.getText().charAt(i)))
            caracSp++;
            else
            caracSp++;
        
        }
        letter=maj+min;
        if(maj<min)
            min=maj;
        if(min>5)
            securityCount+=5;
        else
            securityCount+=min;
        if(letter<number)
            number=letter;
        if(number>5)
            securityCount+=5;
        else
            securityCount+=number;
        if(caracSp>4)
            securityCount+=4;
        else
            securityCount+=number;
        pwdprog.setVisible(true);
                if(securityCount!=0){
                if(securityCount<8)
                {
                	pwdalert.setText("Faible");
                   
                }
                else if (securityCount<15)
                {
                	pwdalert.setText("Moyen");
                }
                else 
                	pwdalert.setText("Fort");
                }
                else
                	pwdalert.setText("");
                pwdprog.setProgress((double) securityCount/21);
     
    }
    
    

    }

   

