package FXMLControllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javafx.scene.layout.AnchorPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.NamingException;
import javafx.scene.text.Text;

import org.controlsfx.control.Notifications;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;



import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.image.ImageView;

import tn.esprit.pidev.entity.Reclamation;
import tn.esprit.pidev.entity.User;

import tn.esprit.pidev.services.ReclamationServiceRemote;
import tn.esprit.pidev.services.UserServiceRemote;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import java.util.Calendar;
import tn.esprit.pidev.entity.Stocks;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Tooltip;

public class ClaimsTraderController implements Initializable {
	public static User userConnected;
	public static String action;

	@FXML
	private TextField trade_id;

	@FXML
	private Button btnSendclaim;

	@FXML
	private TextArea content_area;

	@FXML
	private Button actualizebtn;

	@FXML
	private Text txtname;

    @FXML
    private Button btnlogout;
	@FXML
	private JFXTextField symbole;
	@FXML
	private AnchorPane pane;

	@FXML
	private JFXTreeTableView<Stocks> tableviewstocks;
	ObservableList<Stocks> stocks;
	List<HistoricalQuote> listehq;
	@FXML
	private JFXButton btngo;

	private LineChart<Number, Number> sc;

	ReclamationServiceRemote CS;

	boolean claim = false;
	Reclamation c = new Reclamation();

	private static final int MAX_DATA_POINTS = 50;
	private int xSeriesData = 0;
	private XYChart.Series series1;
	private XYChart.Series series2;
	private XYChart.Series series3;
	private ExecutorService executor;
	private ConcurrentLinkedQueue<Number> dataQ1 = new ConcurrentLinkedQueue<Number>();
	private ConcurrentLinkedQueue<Number> dataQ2 = new ConcurrentLinkedQueue<Number>();
	private ConcurrentLinkedQueue<Number> dataQ3 = new ConcurrentLinkedQueue<Number>();
	private NumberAxis xAxis;
	private AddToQueue addToQueue;

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		Context context = null;
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			String jndiName = "pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
			CS = (ReclamationServiceRemote) context.lookup(jndiName);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// LoginController.userConnected.getId();

		}

	}

	@FXML
	void sendClaimbtn(ActionEvent event) {
		String jndiName = "pidev-ear/pidev-ejb/ReclamationService!tn.esprit.pidev.services.ReclamationServiceRemote";
		Context context = null;
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			CS = (ReclamationServiceRemote) context.lookup(jndiName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		claim = true;

		if (trade_id.getText().isEmpty() || content_area.getText().isEmpty()) {
			claim = false;
			Alert missingFields = new Alert(Alert.AlertType.INFORMATION);
			missingFields.setHeaderText(null);
			missingFields.setContentText("Please complete all fields");
			missingFields.initModality(Modality.APPLICATION_MODAL);
			missingFields.showAndWait();

		}
		if (claim) {

			// c.setClaimDate((java.sql.Date) datepik);
			c.setContent(content_area.getText());
			c.setOption(trade_id.getText());
			CS.ajoutReclamation(c);
			Alert missingFields = new Alert(Alert.AlertType.INFORMATION);
			missingFields.setHeaderText(null);
			missingFields.setContentText("add with succes");
			missingFields.initModality(Modality.APPLICATION_MODAL);
			missingFields.showAndWait();

		}

	}

	@FXML
	void btnActualize(ActionEvent event) {
		trade_id.setText(tableviewstocks.getSelectionModel().getSelectedItem().getValue().toString());

	}

	public void searchforStocksbysymbol(String period, int num) {
		Stock stock = null;
		if (period.equals("none") && num == 0) {
			try {
				action = symbole.getText();
				stock = YahooFinance.get(symbole.getText(), true);
				
			} catch (IOException ex) {

				symbole.requestFocus();
			}
		} else {
			try {
				Calendar from = Calendar.getInstance();
				Calendar to = Calendar.getInstance();
				if (period.equals("Month")) {
					from.add(Calendar.MONTH, -num);
					stock = YahooFinance.get(symbole.getText(), from, to, Interval.MONTHLY);
				} else if (period.equals("Year")) {
					from.add(Calendar.YEAR, -num);
					stock = YahooFinance.get(symbole.getText(), from, to, Interval.WEEKLY);
				} else if (period.equals("Day")) {
					from.add(Calendar.DAY_OF_YEAR, -num);
					stock = YahooFinance.get(symbole.getText(), from, to, Interval.DAILY);
				}

			} catch (IOException ex) {
			
				symbole.requestFocus();
			}
		}

		if (stock != null) {
			System.out.println("currency" + stock.getCurrency());
			System.out.println("name" + stock.getName());
			System.out.println("stock exchange" + stock.getStockExchange());
			System.out.println("symbol " + stock.getSymbol());

			pupulatetableviewstocks(stock);

		}

	}

	@FXML
	void btngoclicked(ActionEvent event) {
		searchforStocksbysymbol("none", 0);

	}

	private void pupulatetableviewstocks(Stock stock) {
		JFXTreeTableColumn<Stocks, String> adjclose = new JFXTreeTableColumn<>("Adj close");
		adjclose.setPrefWidth(150);
		adjclose.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().adjclose;
					}
				});

		JFXTreeTableColumn<Stocks, String> close = new JFXTreeTableColumn<>("Close");
		close.setPrefWidth(150);
		close.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().close;
					}
				});

		JFXTreeTableColumn<Stocks, String> date = new JFXTreeTableColumn<>("Date");
		date.setPrefWidth(150);
		date.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().date;
					}
				});

		JFXTreeTableColumn<Stocks, String> highh = new JFXTreeTableColumn<>("High");
		highh.setPrefWidth(150);
		highh.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().highh;
					}
				});
		JFXTreeTableColumn<Stocks, String> low = new JFXTreeTableColumn<>("Low");
		low.setPrefWidth(150);
		low.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().low;
					}
				});
		JFXTreeTableColumn<Stocks, String> open = new JFXTreeTableColumn<>("Open");
		open.setPrefWidth(150);
		open.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().open;
					}
				});
		JFXTreeTableColumn<Stocks, String> volume = new JFXTreeTableColumn<>("Volume");
		volume.setPrefWidth(150);
		volume.setCellValueFactory(
				new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
						return param.getValue().getValue().volume;
					}
				});

		/////

		stocks = FXCollections.observableArrayList();

		listehq = new ArrayList<HistoricalQuote>();
		try {
			listehq = stock.getHistory();

		} catch (IOException e) {

			e.printStackTrace();
		}
		System.out.println("hhdjdjdjjd");
		for (HistoricalQuote historicalQuote : listehq) {
			System.out.println("symbol " + historicalQuote.getSymbol());
			System.out.println("adj close" + historicalQuote.getAdjClose());
			System.out.println("close " + historicalQuote.getClose());
			System.out.println("date " + historicalQuote.getDate().getTime());
			System.out.println("high" + historicalQuote.getHigh());
			System.out.println("low " + historicalQuote.getLow());
			System.out.println("open " + historicalQuote.getOpen());
			System.out.println("volume " + historicalQuote.getVolume());

		}
		dataQ1.add(Math.random());
		dataQ2.add(0);
		dataQ3.add(0);

		for (HistoricalQuote historicalQuote : listehq) {
			System.out.println("ttttttttttttttttttttttttttttttttttttttttttt");
			System.out.println(historicalQuote.getClose());
			System.out.println("ttttttttttttttttttttttttttttttttttttttttttt");
			if (historicalQuote.getClose() != null) {
				System.out.println("nn");
				System.out.println(historicalQuote.getClose());
				Stocks ltable = new Stocks(historicalQuote.getAdjClose().toString(),
						historicalQuote.getClose().toString(), historicalQuote.getDate().getTime().toString(),
						historicalQuote.getHigh().toString(), historicalQuote.getLow().toString(),
						historicalQuote.getOpen().toString(), historicalQuote.getVolume().toString());
				stocks.add(ltable);
			}
		}
		

		final TreeItem<Stocks> root;
		root = new RecursiveTreeItem<Stocks>(stocks, RecursiveTreeObject::getChildren);
		tableviewstocks.getColumns().setAll(adjclose, close, date, highh, low, open, volume);
		tableviewstocks.setRoot(root);
		tableviewstocks.setShowRoot(false);
		xAxis = new NumberAxis(0, MAX_DATA_POINTS, MAX_DATA_POINTS / 10);
		xAxis.setForceZeroInRange(false);
		xAxis.setAutoRanging(false);

		xAxis.setTickLabelsVisible(false);
		xAxis.setTickMarkVisible(false);
		xAxis.setMinorTickVisible(false);

		NumberAxis yAxis = new NumberAxis();
		yAxis.setAutoRanging(true);

		sc = new LineChart<Number, Number>(xAxis, yAxis) {

			@Override
			protected void dataItemAdded(Series<Number, Number> series, int itemIndex, Data<Number, Number> item) {
			}
		};
		sc.setAnimated(false);
		sc.setId("liveLineeChart");
		sc.setTitle("Animated Line Chart");
		sc.setLayoutX(40);
		sc.setLayoutY(426);
		sc.setPrefHeight(300);
		sc.setPrefWidth(600);
		// -- Chart Series
		series1 = new XYChart.Series<Number, Number>();
		series2 = new XYChart.Series<Number, Number>();
		series3 = new XYChart.Series<Number, Number>();
		sc.getData().addAll(series1, series2, series3);

		pane.getChildren().add(sc);
		executor = Executors.newCachedThreadPool(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setDaemon(true);
				return thread;
			}
		});
		addToQueue = new AddToQueue();
		executor.execute(addToQueue);
		prepareTimeline();
	}

	private class AddToQueue implements Runnable {
		public void run() {
			try {
				Stock stock = YahooFinance.get(symbole.getText(), true);
				List<HistoricalQuote> listehq;
				listehq = new ArrayList<HistoricalQuote>();
				try {
					listehq = stock.getHistory();

				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("hhdjdjdjjd");
				for (HistoricalQuote historicalQuote : listehq) {
					System.out.println("symbol " + historicalQuote.getSymbol());
					System.out.println("adj close" + historicalQuote.getAdjClose());
					System.out.println("close " + historicalQuote.getClose());
					System.out.println("date " + historicalQuote.getDate().getTime());
					System.out.println("high" + historicalQuote.getHigh());
					System.out.println("low " + historicalQuote.getLow());
					System.out.println("open " + historicalQuote.getOpen());
					System.out.println("volume " + historicalQuote.getVolume());

				}

				dataQ1.add(Math.random());
				dataQ2.add(0);
				dataQ3.add(0);

				Thread.sleep(500);
				executor.execute(this);
			} catch (InterruptedException | IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	// -- Timeline gets called in the JavaFX Main thread
	private void prepareTimeline() {
		// Every frame to take any data from queue and add to chart
		new AnimationTimer() {
			@Override
			public void handle(long now) {
				addDataToSeries();
			}
		}.start();
	}

	private void addDataToSeries() {
		for (int i = 0; i < 20; i++) { // -- add 20 numbers to the plot+
			if (dataQ1.isEmpty())
				break;
			series1.getData().add(new AreaChart.Data(xSeriesData++, dataQ1.remove()));
			series2.getData().add(new AreaChart.Data(xSeriesData++, dataQ2.remove()));
			series3.getData().add(new AreaChart.Data(xSeriesData++, dataQ3.remove()));
		}
		// remove points to keep us at no more than MAX_DATA_POINTS
		if (series1.getData().size() > MAX_DATA_POINTS) {
			series1.getData().remove(0, series1.getData().size() - MAX_DATA_POINTS);
		}
		if (series2.getData().size() > MAX_DATA_POINTS) {
			series2.getData().remove(0, series2.getData().size() - MAX_DATA_POINTS);
		}
		if (series3.getData().size() > MAX_DATA_POINTS) {
			series3.getData().remove(0, series3.getData().size() - MAX_DATA_POINTS);
		}
		// update
		xAxis.setLowerBound(xSeriesData - MAX_DATA_POINTS);
		xAxis.setUpperBound(xSeriesData - 1);
	}

    @FXML
    void logoutbtn(ActionEvent event) throws IOException {
    	Stage stage = (Stage) btnlogout.getScene().getWindow();
      	Parent root = FXMLLoader.load(getClass().getResource("/views/connexion.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();

    }
}
